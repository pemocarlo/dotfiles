fish_add_path $HOME/.local/bin

fish_add_path $HOME/flutter/bin

# doom
fish_add_path ~/.emacs.d/bin

# vi keybindings
fish_vi_key_bindings
bind -M insert -m default jk backward-char force-repaint

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`

if type -q direnv
    direnv hook fish | source
end


if type -q pyenv
    pyenv init - | source
    status is-interactive; and pyenv init --path | source
end

alias rm='echo "This is not the command you are looking for."; false'
alias rm -rf ='echo "This is not the command you are looking for."; false'

set -x DOCKER_HOST unix://\$XDG_RUNTIME_DIR/docker.sock

set -Ux JAVA_OPTS '-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'

set -U fish_user_paths /opt/android-sdk/emulator $fish_user_paths
set -U fish_user_paths /opt/android-sdk/platform-tools $fish_user_paths
