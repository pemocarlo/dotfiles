;;; ~/dotfiles/emacs/.doom.d/+agenda.el -*- lexical-binding: t; -*-

(after! org-agenda
  (setq org-agenda-block-separator nil
        org-agenda-start-with-log-mode t)

  (setq org-agenda-sorting-strategy
        `((agenda time-up habit-down priority-down category-keep)
          (todo priority-down category-keep)
          (tags priority-down category-keep)
          (search category-keep))
        )

  (setq org-agenda-files `(,-org-default-projects-dir))

  (defun -org-agenda-process-inbox-item ()
    "Process a single item in the org-agenda."
    (interactive)
    (org-with-wide-buffer
     (org-agenda-set-tags)
     (org-agenda-priority)
     (call-interactively 'org-agenda-set-effort)
     (org-agenda-refile nil nil t)))

  ;; (map! :map evil-org-agenda-mode-map
  ;;       :localleader
  ;;       :n :desc "archive" "aa" #'-org-archive-subtree-as-completed
  ;;       :n :desc "process-inbox" "ii" #'-org-agenda-process-inbox-item)

  (defun -org-archive-subtree-as-completed ()
    "Archives the current subtree to today's current journal entry."
    (interactive)
    ;; According to the docs for `org-archive-subtree', the state should be
    ;; automatically marked as DONE, but I don't notice that:

    (let* ((org-archive-file (or -org-default-completed-file
                                 (todays-journal-entry)))
           (org-archive-location (format "%s::" org-archive-file)))
      (org-agenda-archive)
      ))

  (defun my-org-agenda-skip-all-siblings-but-first ()
    "Skip all but the first non-done entry."
    (let (should-skip-entry)
      (unless (org-current-is-todo)
        (setq should-skip-entry t))
      (save-excursion
        (while (and (not should-skip-entry) (org-goto-sibling t))
          (when (org-current-is-todo)
            (setq should-skip-entry t))))
      (when should-skip-entry
        (or (outline-next-heading)
            (goto-char (point-max)))))
    )

  (defun org-current-is-todo ()
    (string= "TODO" (org-get-todo-state)))

  (setq org-agenda-custom-commands
        `(
          ("z" "Custom view"
           ((agenda ""(
                       (org-agenda-overriding-header "\n⚡ Schedule:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                       (org-agenda-start-day "+0d")
                       (org-agenda-span 2)
                       (org-deadline-warning-days 2)
                       (org-agenda-scheduled-leaders '("" ""))
                       (org-agenda-time-grid (quote ((daily today remove-match)
                                                     (0900 1200 1500 1800 2100)
                                                     "      " "┈┈┈┈┈┈┈┈┈┈┈┈┈")))
                       ))
            (todo "TODO"
                  ((org-agenda-overriding-header "\nTo Refile:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                   (org-agenda-files `(,-org-default-inbox-file))
                   ))
            (todo "NEXT"
                  ((org-agenda-overriding-header "\nThesis:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                   (org-agenda-files `(,(concat -org-default-projects-dir "master.org")))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))
                   ;; (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
                   ))
            (todo "NEXT"
                  ((org-agenda-overriding-header "\nWork:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                   (org-agenda-files `(,(concat -org-default-projects-dir "work.org")))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))
                   ;; (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
                   ))
            (todo "TODO"
                  ((org-agenda-overriding-header "\nOther projects:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                   (org-agenda-files `(,-org-default-projects-file))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))
                   ;; (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
                   ))
            (todo "TODO"
                  ((org-agenda-overriding-header "One-off Tasks:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                   (org-agenda-files `(,-org-default-tasks-file))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))
                   ;; (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
                   ))
            ))

          ("o" "New design"
           ((todo "TODO" (
                          (org-agenda-overriding-header "\n⚡ Do Today:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                          (org-agenda-remove-tags t)
                          (org-agenda-prefix-format " %-2i %-15b")
                          (org-agenda-todo-keyword-format "")
                          ))
            (agenda "" (
                        (org-agenda-start-day "+0d")
                        (org-agenda-span 5)
                        (org-agenda-overriding-header "⚡ Schedule:\n⎺⎺⎺⎺⎺⎺⎺⎺⎺")
                        (org-agenda-repeating-timestamp-show-all nil)
                        (org-agenda-remove-tags t)
                        (org-agenda-prefix-format   "  %-3i  %-15b %t%s")
                        ;; (org-agenda-todo-keyword-format " ☐ ")
                        ;; (org-agenda-current-time-string "⮜┈┈┈┈┈┈┈ now")
                        (org-agenda-scheduled-leaders '("" ""))
                        (org-agenda-time-grid (quote ((daily today remove-match)
                                                      (0900 1200 1500 1800 2100)
                                                      "      " "┈┈┈┈┈┈┈┈┈┈┈┈┈")))
                        ))))
          ))
  )

(setq org-agenda-block-separator (string-to-char " "))
(setq org-agenda-format-date 'my-org-agenda-format-date-aligned)

(defun my-org-agenda-format-date-aligned (date)
  "Format a DATE string for display in the daily/weekly agenda, or timeline.
This function makes sure that dates are aligned for easy reading."
  (require 'cal-iso)
  (let* ((dayname (calendar-day-name date 1 nil))
         (day (cadr date))
         (day-of-week (calendar-day-of-week date))
         (month (car date))
         (monthname (calendar-month-name month 1))
         (year (nth 2 date))
         (iso-week (org-days-to-iso-week
                    (calendar-absolute-from-gregorian date)))
         (weekyear (cond ((and (= month 1) (>= iso-week 52))
                          (1- year))
                         ((and (= month 12) (<= iso-week 1))
                          (1+ year))
                         (t year)))
         (weekstring (if (= day-of-week 1)
                         (format " W%02d" iso-week)
                       "")))
    (format "%-2s. %2d %s"
            dayname day monthname)))


(setq org-clocktable-defaults '(:maxlevel 6 :lang "en" :scope file :block nil
                                :wstart 1 :mstart 1 :tstart nil :tend nil :step nil :stepskip0 nil :fileskip0 t
                                :tags t :match nil :emphasize nil :link nil :narrow 40! :indent t :formula nil
                                :timestamp nil :level nil :tcolumns nil :formatter nil))
